import VueRouter from "vue-router"
import Movies from "../views/Movies.vue"
import Movie from "../views/Movie.vue"
import EditMovie from "../views/EditMovie.vue"

const router = new VueRouter({
  mode: "history",
  routes: [
    {
      path: "/",
      component: Movies
    },
    {
      path: "/movie/:id",
      component: Movie,
      name: "movie"
    },
    {
      path: "/movie/:id/edit",
      component: EditMovie,
      name: "editMovie"
    }
  ]
})

export default router
