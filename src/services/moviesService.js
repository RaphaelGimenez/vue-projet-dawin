import { nanoid } from "nanoid"

export default {
  findAll() {
    const movies = Object.keys(localStorage)
      .filter(key => key.includes("movie"))
      .map(key => JSON.parse(localStorage.getItem(key)))
    return movies
  },
  findOne(id) {
    return JSON.parse(localStorage.getItem(this.generateKey(id)))
  },
  create(movie) {
    const id = nanoid()
    localStorage.setItem(this.generateKey(id), JSON.stringify({ ...movie, id }))
  },
  update(movie) {
    localStorage.setItem(this.generateKey(movie.id), JSON.stringify(movie))
    return movie
  },
  delete(id) {
    localStorage.removeItem(this.generateKey(id))
  },
  generateKey(id) {
    return `movie_${id}`
  }
}
