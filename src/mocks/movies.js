export default [
  {
    title: "Les évadés",
    date: 1994,
    lang: "fr",
    description:
      "Two imprisoned men bond over a number of years, finding solace and eventual redemption through acts of common decency.",
    genre: "drama",
    poster:
      "https://m.media-amazon.com/images/M/MV5BMDFkYTc0MGEtZmNhMC00ZDIzLWFmNTEtODM1ZmRlYWMwMWFmXkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_.jpg",
    rating: 4.5,
    director: {
      fullName: "Frank Darabont",
      birthDate: "1959-01-28",
      nationality: "French"
    }
  },
  {
    title: "Le parrain",
    date: 1972,
    lang: "fr",
    description:
      "The aging patriarch of an organized crime dynasty transfers control of his clandestine empire to his reluctant son.",
    genre: "crime, drama",
    poster:
      "https://m.media-amazon.com/images/M/MV5BM2MyNjYxNmUtYTAwNi00MTYxLWJmNWYtYzZlODY3ZTk3OTFlXkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_.jpg",
    rating: 5,
    director: {
      fullName: "Francis Ford Coppola",
      birthDate: "1939-04-07",
      nationality: "Américain"
    }
  },
  {
    title: "The Dark Knight: Le chevalier noir",
    date: 2008,
    lang: "en",
    description:
      "When the menace known as the Joker wreaks havoc and chaos on the people of Gotham, Batman must accept one of the greatest psychological and physical tests of his ability to fight injustice.",
    genre: "action, Crime, Drama ",
    poster:
      "https://m.media-amazon.com/images/M/MV5BMTMxNTMwODM0NF5BMl5BanBnXkFtZTcwODAyMTk2Mw@@._V1_.jpg",
    rating: 3.5,
    director: {
      fullName: "Christopher Nolan",
      birthDate: "1970-07-30",
      nationality: "Américain"
    }
  }
]
