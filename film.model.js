const film = {
  title: String,
  date: Number,
  lang: String,
  description: String,
  director: {
    fullName: String,
    nationality: String,
    birthDate: Number
  },
  genre: String,
  poster: String
}

class Film {
  constructor(title, date, lang, director, genre, poster) {
    this.title = title
    this.date = date
    this.lang = lang
    this.director = director
    this.genre = genre
    this.poster = poster
  }
}

export default { film, Film }
